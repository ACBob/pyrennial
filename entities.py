import pygame
import loading
import renderutil
from OpenGL import GL as opengl

pygame.init()

class InWorld(object):

    def __init__(self,x,y,z):
        self.position = (x,y,z)

class MDL(InWorld):

    def __init__(self,filename,tex=None,x=0,y=0,z=0):
        super().__init__(x,y,z)
        self.filename = filename
        self.mdl_data = loading.load_obj(filename)
        self.tex = tex

    def draw(self):
        if self.tex:
            opengl.glBindTexture(opengl.GL_TEXTURE_2D, self.tex)
        renderutil.Draw(self.mdl_data,self.position)
        if self.tex:
            opengl.glBindTexture(opengl.GL_TEXTURE_2D, 0)

class AxisRep(MDL):

    def __init__(self,x=0,y=0,z=0):
        super().__init__(x,y,z)
        #This uses the TexCoords as Colours instead
        self.mdl_data = [[[0,0,0],[1,0,0],[0,1,0],[0,0,1]],[[0,1],[0,2],[0,3]],[(1,1,1),(1,0,0),(0,1,0),(0,0,1)],[]]

        self.filename = None
        self.tex = None

    def draw(self):
        opengl.glBegin(opengl.GL_LINES)

        for i in self.mdl_data[1]:
            for j in i:
                opengl.glColor3fv(self.mdl_data[2][j])
                opengl.glVertex3fv(self.mdl_data[0][j])
        
        opengl.glEnd()
        opengl.glColor3f(1,1,1)
