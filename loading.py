import pygame
from OpenGL import GL as opengl
pygame.init()
_texcache = {}

#Load OBJ
def load_obj(filepath):
    """
        @return (Faces, Vertices, TexCoord, Normals)
    """

    objfile = open(filepath,'r')
    objtext = objfile.readlines()
    objfile.close()

    verts = []
    texvt = []
    faces = []
    norms = []

    for line in objtext:
        #print(line)
        line=line.strip()
        if line[0] == "#":
            continue #It's a comment
        elif line[0:2] == "vt":
            #print("TexCoord!")
            line = line.split()[1:]
            line = list(map(float, line)) #I like this, python.

            texvt.append(line)
            continue
        elif line[0:2] == "vn":
            #print("Vertex Normal!")
            continue
        elif line[0] == "v":
            #print("Vertex!")
            line = line.split()[1:]
            line = list(map(float, line)) #I like this, python.
            #print(line)

            verts.append(line)
                                
            continue #We're done processing this line
        elif line[0] == "f":
            #print("Face!")
            line = line.split()[1:]

            face = []

            for indice in line:
               # if indice.find("//"):
               #     #It's a Vertex Normal thingie fuck
               #     #TODO: HANDLE NORMALS
               #     indice = indice.split('//')
               #     face.append(int(indice[0]))
               #     continue
                if indice.find("/"):
                    #It's a Vertex Texture Coordinate Indice
                    #TODO: HANDLE TEXTURE COORDS BETTER
                    indice = indice.split('/')
                    face.append([int(indice[0])-1,int(indice[1])-1,int(indice[2])-1])
                else:
                    face.append([int(indice)-1,0,0])

            faces.append(face)

            continue #We're done processing this line

        print("Unreachable state on",line)


    return [faces,verts,texvt,norms]

def load_tex(filename):
    if filename in _texcache:
        return _texcache[filename]
    texsurf = pygame.image.load(filename)
    texdata = pygame.image.tostring(texsurf,'RGBA', 1)
    w, h = texsurf.get_width(), texsurf.get_height()

    opengl.glEnable(opengl.GL_TEXTURE_2D)
    texid = opengl.glGenTextures(1)

    opengl.glBindTexture(opengl.GL_TEXTURE_2D, texid)
    opengl.glTexImage2D(opengl.GL_TEXTURE_2D, 0, opengl.GL_RGB, w, h, 0, opengl.GL_RGBA, opengl.GL_UNSIGNED_BYTE, texdata)

    opengl.glTexParameterf(opengl.GL_TEXTURE_2D, opengl.GL_TEXTURE_WRAP_S, opengl.GL_REPEAT)
    opengl.glTexParameterf(opengl.GL_TEXTURE_2D, opengl.GL_TEXTURE_WRAP_T, opengl.GL_REPEAT)
    opengl.glTexParameterf(opengl.GL_TEXTURE_2D, opengl.GL_TEXTURE_MAG_FILTER, opengl.GL_NEAREST)
    opengl.glTexParameterf(opengl.GL_TEXTURE_2D, opengl.GL_TEXTURE_MIN_FILTER, opengl.GL_NEAREST)

    opengl.glBindTexture(opengl.GL_TEXTURE_2D, 0)
    _texcache[filename] = texid
    return texid
