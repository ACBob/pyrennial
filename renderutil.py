import pygame
import loading
from OpenGL import GL as opengl
from OpenGL.GL import shaders as openglshaders

import numpy

global shaders
shaders = {}
VAO = 0

def Draw_Old(mdl,position=(0,0,0)):
    opengl.glBegin(opengl.GL_TRIANGLES)

    for face in mdl[0]:
        #print(face)
        for vertex in face:
            #print(vertex-1)
            #print(mdl[1][vertex-1])
            coordind = vertex[0]
            texind = vertex[1]
            opengl.glTexCoord2fv(mdl[2][texind])
            opengl.glVertex3fv(AddLists(mdl[1][coordind],position))

    opengl.glEnd()

def Draw(*_):
    UseShader('GEO')

    UseNoShader()

def Init():
    global shaders
    test_mdl = numpy.array(loading.load_obj("res/mdl/test.obj")[0], dtype=numpy.float32)

    geo_shaderv = load_shader("shaders/vertex.glsl", openglshaders.GL_VERTEX_SHADER)
    geo_shaderf = load_shader("shaders/fragment.glsl", openglshaders.GL_FRAGMENT_SHADER)
    
    shaders['GEO'] = compile_shader_program(geo_shaderv, geo_shaderf)

    VAO = opengl.glGenVertexArrays(1)
    opengl.glBindVertexArray(VAO)

    #TODO: Vertex Buffer, ideally the MDL should do it.
    VBO = opengl.glBindBuffers(1)
    opengl.glBindBuffer(opengl.GL_ARRAY_BUFFER, VBO)
    opengl.glBufferData(opengl.GL_ARRAY_BUFFER, test_mdl.nbytes, test_mdl, opengl.GL_STATIC_DRAW)

    position = opengl.glGetAttribLocation(shaders['GEO'], 'position')

    opengl.glEnableVertexAttribArray(0)
    opengl.glVertexAttribPointer(position, 3, opengl.GL_FLOAT, opengl.GL_FALSE, 20, None)

    opengl.glBindBuffer(opengl.GL_ARRAY_BUFFER, 0)
    opengl.glBindVertexArray(0)

def UseShader(shader):
    opengl.glUseProgram(shaders[shader])

def UseNoShader():
    opengl.glUseProgram(0)

def AddLists(a,b):
    return [ x + y for x,y in zip(a,b)]

def load_shader(filepath,shadertype):
    file = open(filepath)
    shaderlines = file.read()
    file.close()

    shader = openglshaders.compileShader(shaderlines, shadertype)

    return shader

def compile_shader_program(vertex, fragment):

    shaderprogram = openglshaders.compileProgram(vertex, fragment)
