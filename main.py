import pygame
import sys
import loading
import entities
from pygame.locals import *
from OpenGL import GL as opengl
from OpenGL import GLU as glu

import random
import renderutil

#TESTING LOADOBJ
#print(load_obj("/tmp/test.obj"))
#sys.exit(0)

#OpenGL Test Function

    

pygame.init()
pygame.display.init()

display = pygame.display.set_mode((800,600), pygame.OPENGL|pygame.DOUBLEBUF)

#BEGIN OPENGL SETUP
opengl.glDepthMask(True)
opengl.glDepthFunc(opengl.GL_LESS)
opengl.glEnable(opengl.GL_DEPTH_TEST)
opengl.glCullFace(opengl.GL_BACK)
#END OPENGL SETUP

#BEGIN GLU SETUP
glu.gluPerspective(90, (800/600), 0.1, 50.0)
#END GLU SETUP

renderutil.Init()

#End of PreLoop
running = True
opengl.glTranslatef(0, 0, -5)

test_texture = loading.load_tex("res/tex/testtex.png")
test_cubes = [entities.MDL("res/mdl/nugget.obj",test_texture,0,5,0)]
test_cubes.append(entities.MDL("res/mdl/nugget.obj",test_texture,0,-5,0))
test_cubes.append(entities.MDL("res/mdl/nugget.obj",test_texture,5,0,0))
test_cubes.append(entities.MDL("res/mdl/nugget.obj",test_texture,-5,0,0))
test_cubes.append(entities.MDL("res/mdl/nugget.obj",test_texture,0,0,5))
test_cubes.append(entities.MDL("res/mdl/nugget.obj",test_texture,0,0,-5))
axis = entities.AxisRep()

game_clock = pygame.time.Clock()
while running:
    opengl.glClearColor(0.7,0.3,0.8,1)
    opengl.glClear(opengl.GL_COLOR_BUFFER_BIT|opengl.GL_DEPTH_BUFFER_BIT)
    events = pygame.event.get()
    for event in events:
        if event.type == pygame.QUIT:
            running = False
            break

    keys = pygame.key.get_pressed()

    if keys[pygame.K_w]:
        opengl.glTranslatef(0.0,0.0,0.1)
    if keys[pygame.K_s]:
        opengl.glTranslatef(0.0,0.0,-0.1)
    if keys[pygame.K_a]:
        opengl.glTranslatef(0.1,0.0,0.0)
    if keys[pygame.K_d]:
        opengl.glTranslatef(-0.1,0.0,0.0)

    if keys[pygame.K_RIGHT]:
        opengl.glRotatef(-1.0,0.0,1.0,0.0)
    if keys[pygame.K_LEFT]:
        opengl.glRotatef(1.0,0.0,1.0,0.0)

    for i in test_cubes:
        i.draw()
    
    opengl.glDepthFunc(opengl.GL_ALWAYS)
    opengl.glDisable(opengl.GL_DEPTH_TEST)
    axis.draw()
    opengl.glEnable(opengl.GL_DEPTH_TEST)
    opengl.glDepthFunc(opengl.GL_LESS)

    pygame.display.flip()
    game_clock.tick(60)
    pygame.display.set_caption(str(game_clock.get_fps()))

pygame.quit()
